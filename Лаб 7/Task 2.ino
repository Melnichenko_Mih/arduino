int sensorPin = A5; //задание аналогового входа А0
int sensorValue = 0;
int maxVal = 1023;
int voltage = 5; //напряжение
float temp;
float voltageOut;

void setup() {
  Serial.begin(9600); //инициализация последовательного порта
}

void loop() {
  // чтение значения АЦП (состояния датчика)
  sensorValue = analogRead(sensorPin);
  //вывод значения в порт (монитор порта)
  voltageOut = (sensorValue / 1023.f) * 5;
  temp = (voltageOut * 1000) / 10;
  
  Serial.print("ADC code: ");
  Serial.println(sensorValue);
  Serial.print("Output voltage: ");
  Serial.println(voltageOut);
  Serial.print("Temperature: ");
  Serial.println(temp);
  delay(500); //задержка на считывание состояния АЦП
}
