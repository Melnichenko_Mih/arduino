int voicePin = 13;
int timeVal = 200;

void setup()
{
  pinMode(voicePin, OUTPUT);
}

void loop()
{
  	for(int i = 0; i < 3; i++) 
    {
     point();
    }
  	delay(timeVal*3);
  	for(int i = 0; i < 3; i++) 
    {
     line();
    }
  	delay(timeVal*3);
  	for(int i = 0; i < 3; i++) 
    {
     point();
    }
  	delay(timeVal*7);
}
void point()
{
  digitalWrite(voicePin,HIGH);
  delay(timeVal);
  digitalWrite(voicePin,LOW);
  delay(timeVal);
}
void line()
{
  digitalWrite(voicePin,HIGH);
  delay(timeVal*3);
  digitalWrite(voicePin,LOW);
  delay(timeVal);
}