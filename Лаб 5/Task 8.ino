char incomingByte = 0;
int vibroPin = 9;
int ledPin = 13;
int soundPin = 3;

void setup()
{
  pinMode(vibroPin, INPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(soundPin, OUTPUT);
  Serial.begin(9600);
}

void loop()
{
  if(digitalRead(vibroPin) == LOW)
  {
    digitalWrite(ledPin,HIGH);
    delay(200);
  	digitalWrite(ledPin,LOW);
  	delay(200);
    digitalWrite(soundPin,HIGH);
    Serial.println("ERROR");
  }
  else {
    digitalWrite(soundPin,LOW);
  }
}
