int ledPin = 13;
int timeVal = 200;

void setup()
{
  pinMode(ledPin, OUTPUT);
}

void loop()
{
  	for(int i = 0; i < 3; i++) 
    {
     point();
    }
  	delay(timeVal*3);
  	for(int i = 0; i < 3; i++) 
    {
     line();
    }
  	delay(timeVal*3);
  	for(int i = 0; i < 3; i++) 
    {
     point();
    }
  	delay(timeVal*7);
}
void point()
{
  digitalWrite(ledPin,HIGH);
  delay(timeVal);
  digitalWrite(ledPin,LOW);
  delay(timeVal);
}
void line()
{
  digitalWrite(ledPin,HIGH);
  delay(timeVal*3);
  digitalWrite(ledPin,LOW);
  delay(timeVal);
}